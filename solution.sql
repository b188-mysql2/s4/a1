-- Find all artists with the letter d in their names
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that have a length less than 2:30
SELECT * FROM songs WHERE length < 230;

-- Join the albums and songs table (Only show album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs on albums.id = songs.album_id;

-- Join the artists and albums tables
-- Find all albums that have the letter 'A' in its name
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- Sort the albums in Z-A order, show only the first 4 records
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs tables. Sort albums from Z-A and sort songs from A-Z.
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC,song_name ASC;
